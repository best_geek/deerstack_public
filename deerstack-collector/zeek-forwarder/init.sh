#!/bin/bash
echo "starting container zeek forwarder"

echo "configuring filebeat"
tee -a /etc/filebeat/filebeat.yml >/dev/null << EOF
#### zeek forwarder config 
output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["${ELASTIC_HOST}:${ELASTIC_PORT}"]
  ssl.verification_mode: none
  path: ${ELASTIC_PATH}
  protocol: "https"
  index: 'deerstack-%{[agent.version]}-%{+dd.MM.yyyy}'

  # Authentication credentials - either API key or username/password.
  #api_key: "id:api_key"
  username: "elastic"
  password: ${ELASTIC_PASSWORD}
setup.ilm.enabled: false 
setup.template.name: "deerstack"
setup.template.pattern: "deerstack-*"
EOF

echo "configured filebeat OK"


echo "changing perms zeek config file"
chmod 600 /etc/filebeat/modules.d/zeek.yml
echo "changed perms on zeek file"

#tail -f /dev/null

echo "starting filebeat"
filebeat -e -v -E "name=${COLLECTOR_NAME}"


