#!/bin/bash
echo "Listening interface: ${LISTEN_INTERFACE}"

echo "installing zeek"
#install zeek
apt-get update -y && \
	apt-get install zeek-lts -y -o Dpkg::Options::="--force-confdef" && \ 
	rm -rf /var/cache/apt/archives

#generate zeek node conf and bail if not successful
echo "generating new zeek node config file"
python3 gen_node_conf.py
if [ $? -ne 0 ]
then
	echo "issues generating zeek node config file"
	exit 1
fi
cp /tmp/node.cfg /opt/zeek/etc
echo "editied config file OK"

echo "editing zeek to output json"
echo "@load policy/tuning/json-logs.zeek" >> /opt/zeek/share/zeek/site/local.zeek

echo "starting zeek"
/opt/zeek/bin/zeekctl deploy
echo "done"

echo "zeek status:"
/opt/zeek/bin/zeekctl status



#keep container away 
while true
do
	tail -f /dev/null
done
