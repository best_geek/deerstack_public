#!/bin/bash
CONFIG_PATH="/etc/deerstack/server"
CONFIG_FILE="server.conf"
INSTALLER_LOCATION="/opt/deerstack/server"
VERSION=1



# function to check the state of a docker container
function check_dkr_state () {
   DKR_STATE=$(docker inspect --format='{{json .State.Status}}' $1)
   if [[ $! -ne 0 ]]
    then
        echo -e "\xE2\x9C\x97 $1 failed to start. Review ouput. Exiting"
        exit 1
    fi

    if ! (echo ${DKR_STATE} | grep -q "running")
    then
        echo -e "\xE2\x9C\x97 $1 failed to start. Review ouput. Exiting"
        exit 1
    fi
}



clear
echo "Running installation for deerstack-server"
echo "Installer ver: ${VERSION}"

#check which version of SED
#BSD versions of sed make -i different. 
if (strings $(which sed) | grep -q 'GNU sed')
then
    SED_RPLCE_CMD="$(which sed) -i"
    echo "[i] GNU sed present" 
else
    SED_RPLCE_CMD="$(which sed) -i .orignal"
    echo "[i] Default to BSD sed" 

fi

#check running as root
if [ "$EUID" -ne 0 ]
then 
    echo -e "\xE2\x9C\x97 root check failed"
    echo "Please run as root"
    exit
else
    echo -e "\xE2\x9C\x94 running as root"
fi

#do precheck requirements
which docker-compose > /dev/null 
if [[ $! -ne 0 ]]
then
    echo -e "\xE2\x9C\x97 docker-compose is not installed. Exiting"
else
    echo -e "\xE2\x9C\x94 docker-compose installed"
fi

#check if existing config file
if [ -f ${CONFIG_PATH}/${CONFIG_FILE} ]
then
    echo -e "\xE2\x9C\x94 exisiting configuration file detected"
    EXISTING_CONF=1
else
    echo "- New installation"
    EXISTING_CONF=0
fi

#make runtime directories
sudo mkdir -p ${CONFIG_PATH}
sudo mkdir -p ${INSTALLER_LOCATION}
echo -e "\xE2\x9C\x94 ensure required directories exist"

#copy required files to installation directory
sudo cp -r ./deerstack-server/* ${INSTALLER_LOCATION}
echo -e "\xE2\x9C\x94 copied files to installation directory"

#existing config
if [[ ${EXISTING_CONF} -eq 1 ]]
then 
    #source our config options 
    source ${CONFIG_PATH}/${CONFIG_FILE}
    if [[ $! -ne 0 ]]
    then
        echo -e "\xE2\x9C\x97 loading existing config options failed"
        echo "Please manually remove ${CONFIG_PATH}"/${CONFIG_FILE}

    fi
    echo -e "\xE2\x9C\x94 loaded existing config options"

else
    #grab user input
    printf "[-] Please specify server password. This will also be used for collectors: "
    read -s INPUT_ELASTIC_PASSWORD

    printf "\n[-] Please specify number of days to retain logs: "
    read INPUT_RETENTION_DAYS
fi

#if we didn't have config now make one
if [[ ${EXISTING_CONF} -ne 1 ]]
then
    echo "# Config file generated on $(date)" | sudo tee ${CONFIG_PATH}/${CONFIG_FILE} > /dev/null
    echo "INPUT_ELASTIC_PASSWORD=${INPUT_ELASTIC_PASSWORD}"| tee -a ${CONFIG_PATH}/${CONFIG_FILE}  > /dev/null
    echo "INPUT_RETENTION_DAYS=${INPUT_RETENTION_DAYS}" | tee -a ${CONFIG_PATH}/${CONFIG_FILE}  > /dev/null
    sudo chmod 600 ${CONFIG_PATH}/${CONFIG_FILE}
    echo " "
    echo -e "\xE2\x9C\x94 saved config options"
fi

#replace config options with sed
#note this doesn't work on BSD versions such as MacOS
$SED_RPLCE_CMD "s/ELASTIC_PASSWORD: changeme/ELASTIC_PASSWORD: ${INPUT_ELASTIC_PASSWORD}/g" ${INSTALLER_LOCATION}/docker-compose.yaml
$SED_RPLCE_CMD "s/ELASTICSEARCH_PASSWORD: changeme/ELASTICSEARCH_PASSWORD: ${INPUT_ELASTIC_PASSWORD}/g" ${INSTALLER_LOCATION}/docker-compose.yaml
$SED_RPLCE_CMD "s/RETENTION_DAYS: 100/RETENTION_DAYS: ${INPUT_RETENTION_DAYS}/g" ${INSTALLER_LOCATION}/docker-compose.yaml
echo -e "\xE2\x9C\x94 configured docker-compose.yaml"

#start to build the container
cd ${INSTALLER_LOCATION}
echo "[..] Start build of sever containers ... This may take time (5mins+) "
docker-compose build -q
if [[ $! -ne 0 ]]
then
    echo -e "\xE2\x9C\x97 Container failed to build. Exiting. Submit logs for build issues"
else
    echo -e "\xE2\x9C\x94 image built successfully"
fi


#start to run container
#already in build working directory
echo "[..] Start server containers"
docker-compose up -d --force-recreate
if [[ $! -ne 0 ]]
then
    echo -e "\xE2\x9C\x97 Container failed to start. Exiting. Submit logs for build issues"
else
    echo -e "\xE2\x9C\x94 issue docker-compose up command"
fi

#make sure container state is running so add sleep time
echo "[..] sleep to wait for services to come up"
secs=$((2 * 60))
while [ $secs -gt 0 ]; do
   echo -ne "\r"
   printf  "$secs"
   echo -ne " seconds$pc\033[0K"
   sleep 1
   : $((secs--))
done

#check all is up 
check_dkr_state deerstack-elastic
check_dkr_state deerstack-kibana
check_dkr_state deerstack-proxy


echo " "
echo -e "\xE2\x9C\x94 Services up and running"
echo -e "\xE2\x9C\x94 Installation succeeded!"
