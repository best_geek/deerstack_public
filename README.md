# Deerstack


## Overview 

### Introduction
What is Deerstack? Deerstack is an absolutely fabulous play on words due to the underlying technologies being based on the elastic stack or ELKstack. As a fun project, I wanted to be able to search through all network traffic and use the Zeek project as a way to capture this traffic, to then get sent to an elastic backend. If you want to just get it going, skip straight to the [Deployment](#Deployment) section. 

If you're looking for an in-depth breakdown of this project, please visit my blog [here](https://me.webbhome.net/projects/deerstack/) 

## Deployment
### Hardware / Software Recommendations
The below covers the server AND collector:
Debian based OS (the installation script supports MacOS but has not been extensively tested)
- Docker
- Docker-compose
- bash
All of the above are easily obtainable.
 
#### Server
- Dual core processor
- \> 512MiB of RAM (if you have more and you wish to take advantage you can override `ES_JAVA_OPTS: "-Xmx256m -Xms256m"` in docker-compose but note you'll have to make this change every time you run the deployment script at `/opt/deerstack/server/docker-compose.yaml`)
- 10GiB + disk space
 
#### Collector
- x86/x64 compatible processor
- 5GiB of disk space (though should be traffic load will determine how much storage you actually 'need')
- \> 512MiB of RAM
 
### Installation:
### Server
- Clone the repository
- (Recommended) Replace server certificates at `deerstack-server/proxy/certs` by running `./create_certs.sh`
- Ensure you're in the root of the repository
- Run `sudo ./server_installer.sh` which will run required pre-checks and require you to fill in information required
*If you ever need to reset this configuration, remove `/etc/deerstack/server/server.conf`*
- Load your browser pointing to `https://<ip>:8088`. You may need to refresh on first run as the kibana server may not be fully ready
- If you are having issues, make sure the installation script completed successfully and did not present any errors
 
**NOTE**: In redeploying, you will lose your saved data. I analyse how you could get around this in my full blogpost.

### Collector
#### Considerations for where to install the collector:
You can install the collector on anything that meets the requirement. It can even monitor multiple interfaces. Think of the best place to deploy the collector. For example, my firewall sits in a VM; so I have created a monitoring VM connected to a VSwitch where the port group includes all VLANs for maximum capture. I plan to do a full writeup about this later on. 

If you were just interested in a particular server's traffic, you could just install it on one or two servers. You can identify unique logs from collectors by their names in kibana. This is dictated by whatever you called them in the collector installation script.
 
#### Install instructions:
- Clone the repository
- Ensure you're in the root of the repository
- Make sure to have run the server installer above
- Run `sudo ./collector_installer.sh` which will run required pre-checks and require you to fill in information required
*If you ever need to reset this configuration, remove `/etc/deerstack/collector/collector.conf`*
- Check your data is being ingested (which can take up to 10 minutes). You can manually search on the server with `agent.name: "<collector_name_from_install>" `
 
## Troubleshooting:
In building this project, there were a number of things that went wrong and I had to troubleshoot. Whilst most are now d, I have left some helpful troubleshooting steps and this project is not 'bug free'. Troubleshooting steps are broken into the server and collector components:
### Server:
In any situations, the following containers should be up and can be discovered with issuing `docker ps -a`:
```
e4a5b07bf823   server_setup-elastic       "./init.sh"              21 hours ago   Up 21 hours                                                              deerstack-setup-elastic
3839abaf2c7a   server_apache              "./init.sh"              21 hours ago   Up 21 hours (healthy)   0.0.0.0:8088->443/tcp                            deerstack-proxy
c798a19706ce   server_kibana              "/bin/tini -- /usr/l…"   21 hours ago   Up 21 hours             5601/tcp                                         deerstack-kibana
549fabac61d9   server_elasticsearch       "/bin/tini -- /usr/l…"   21 hours ago   Up 21 hours             0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp   deerstack-elastic
```

- Kibana Server is not ready yet: wait and refresh the page is 5 minutes
- Licensing errors printed on screen: restart the kibana container manually with `docker stop deerstack-kibana; docker start deerstack-kibana`
- Proxy errors in browser: if just deployed, wait 5 minutes. Else, restart kibana manually with `docker stop deerstack-kibana; docker start deerstack-kibana`
- Last resort: Look for errors in the runtime logs as (most are quite self explanatory). Can be achieved with `cd /opt/deerstack/server; docker-compose logs -f`
- You're seeing duplicate traffic logs: you have probably misconfigured the collector. Redeploy the container, making sure to remove `/etc/deerstack/collector/collector.conf` first. Ensure you follow the deployment questions carefully
- You're prompted to create a 'New index pattern': Occurs if the elastic container takes too long to come up. (if you're getting this error, probably an indication your hardware is too slow). However, can be manually fixed with `docker stop deerstack-setup-elastic; docker start deerstack-setup-elastic`
- A last resort, restart everything: `cd /opt/deerstack/server; docker-compose stop; docker-compose up -d`
 
### Collector:
Make sure to give the collector 5-10 minutes before concerning yourself it isn't working. 

Many issues on the collector can be diagnosed by looking at the logs of the container with `cd /opt/deerstack/collector; docker-compose logs`. 

Compare troubleshooting steps below from the output of the above command
 
- `/init.sh: line 29: /opt/zeek/bin/zeekctl: No such file or directory` : you have probably installed on a non-supported architecture. Run `docker stop deerstack-zeek-listener-1; docker start deerstack-zeek-listener-1` to be sure and review logs after
- `Error: error occurred while trying to send mail: sendmail: Cannot open mailhub:25`: This is normal because the ssmtp package never gets configured and I don't intend on doing this
- Ensure you see some output like the following to verify zeek is actually running:
 
```
zeek status:
Warning: failed to find local IP addresses with "ifconfig -a" or "ip address" commands
Name         Type    Host             Status    Pid    Started
logger-1     logger  localhost        running   2309   28 Dec 21:47:36
manager      manager localhost        running   2679   28 Dec 21:47:38
proxy-1      proxy   localhost        running   3276   28 Dec 21:47:40
worker-1     worker  localhost        running   3514   28 Dec 21:47:41
```
 
- ensure you see the following in the logs `editied config file OK`. If you don't, remove the config file in `/etc/deerstack/collector/collector.conf` and re-run the installation script
- A last resort, restart everything: `cd /opt/deerstack/collector; docker-compose stop; docker-compose up -d`
 
 
### Licensing:
I am no licensing expert. I *think* I have used stuff which is free but please do not re-distribute any of my custom code. The technologies used are listed at the begining. The user must take it upon themselves to review licensing for each. 

### Maintaining:
The objective of this repo is to demonstrate a technical ability and to review room for improvements. I have this installed on a personal network with no intention to 'actively' maintain this. Future tweaks may get pushed. 

### Security:
Any security involved is only as good as the software I've used. I make no statement that this project is 'secure' or 'insecure'. 