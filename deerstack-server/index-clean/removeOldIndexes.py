import json
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import os
import sys
import pprint as pp
from datetime import datetime, timedelta

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


RETENTION_DAYS=int(os.environ['RETENTION_DAYS'])
ELASTIC_HOST="deerstack-elastic"
ELASTIC_PORT="9200"
ELASTIC_URL=f"http://{ELASTIC_HOST}:{ELASTIC_PORT}"
ELASTIC_USER=os.environ['ELASTICSEARCH_USERNAME']
ELASTIC_PASSWORD=os.environ['ELASTICSEARCH_PASSWORD']
CREDS=(ELASTIC_USER,ELASTIC_PASSWORD)
FAIL_FILE="/tmp/indexfail"

# run some pre-checks
if not ELASTIC_HOST:
    raise RuntimeError("Cannot proceed without a defined elasticsearch hostname")

if not ELASTIC_PASSWORD:
    raise RuntimeError("Cannot proceed without a defined elasticsearch port")

if not ELASTIC_USER:
    raise RuntimeError("Cannot proceed without a defined elasticsearch username")

if not ELASTIC_PASSWORD:
    raise RuntimeError("Cannot proceed without a defined elasticsearch password")


def calculateDatesToKeep(deltaDays=None,fmt="%d.%m.%Y"):
    """Calculates a list of datetime formats to keep, based on timedelta
    Args:
        deltaDays (_type_, required): Number of days to keep. Defaults to None.
        fmt (str, optional): strftime . Defaults to "%d.%m.%Y".

    Raises:
        RuntimeError: When no deltadays specified

    Returns:
        list: days to keep, specified in the fmt option
    """

    if not deltaDays:
        raise RuntimeError("No time delta specified")

    toKeep=[]
    now = datetime.now()

    for i in range(0,deltaDays):
        # iterate from now and work out each format to keep
        delta = now - timedelta(days=i)
        delta=delta.strftime(fmt)
        toKeep.append(delta)

    return toKeep


def getAllIndexes(fullUrl="",credentials=("","")):
    """Will obtain all indexes located within an elastic instance

    Args:
        fullUrl (str, required): full URL of cluster, excluding /_cat/indices. Defaults to "".
        credentials (tuple, optional): authentication to send with request Defaults to ("","").

    Raises:
        RuntimeError: When a full URL is not provided
        RuntimeError: Exception for when a request cannot be made by 'requests'
        RuntimeError: Maximum tries were exceeded for querying the cluster

    Returns:
        _type_: Text from requests.text
    """

    if not fullUrl:
        raise RuntimeError("Cannot make requests without full URL")

    # add our param
    fullUrl+="/_cat/indices"

    maxTries=5
    tries=0
    while tries < maxTries:
        tries+=1

        try:
            r = requests.get(fullUrl,auth=credentials,verify=False,timeout=5)
        except:
            raise RuntimeError("Could not make request")

        if r.status_code == 200:
            return r.text

    touchFileError()
    raise RuntimeError("Exceeded max tries when requesting current indexes")


def calIndexesRemoval(keepDates=[],deerStackIndexes=[]):
    
    if not keepDates:
        raise ValueError("Must at least have one day to keep for index name")
    if not deerStackIndexes:
        raise ValueError("Must at least have one deerStack Index to filter on")

    markedRemoval=[]

    # ['yellow open deerstack-7.16.2-02.04.2022     2W8nxQu6TrON0OCY9O9MvQ 1 1 28328    0  19.2mb  19.2mb']
    # work out if for each index, it contains the dates we need to keep
    # if they don't, return a list with the indexes to remove
    for i in deerStackIndexes:
        name=i.split(" ")[2]

        # if we hit a match, break out of the loop
        for date in keepDates:
            if date in name:
                break
            
            # we haven't passed
            if name not in markedRemoval:
                markedRemoval.append(name)

    return markedRemoval


def deleteIndexByName(fullUrl="",credentials=("",""), indexName=""):
    """Will remove index by index name elastic

    Args:
        fullUrl (str, required): full URL of cluster, excluding /_cat/indices. Defaults to "".
        credentials (tuple, optional): authentication to send with request Defaults to ("","").
        indexName (str, required): The index name to remove.


    Raises:
        RuntimeError: When a full URL is not provided
        ValueError: The indexName cannot be blank
        RuntimeError: Exception for when a request cannot be made by 'requests'
        RuntimeError: Maximum tries were exceeded for querying the cluster

    Returns:
        _type_: Text from requests.text
    """

    if not fullUrl:
        raise RuntimeError("Cannot make requests without full URL")

    if not indexName:
        raise ValueError("Index to delete cannot be blank")

    # add our param
    fullUrl+=f"/{indexName}"

    maxTries=5
    tries=0
    while tries < maxTries:
        tries+=1

        try:
            r = requests.delete(fullUrl,auth=credentials,verify=False)
        except:
            raise RuntimeError("Could not make request")

        if r.status_code == 200:
            return r.text

    raise RuntimeError(f"Exceeded max tries when trying to remove index {indexName}")

def touchFileError(fp=FAIL_FILE):
    with open(fp, 'a'):     # Create file if does not exist
        os.utime(fp, None)

print("# INDEX REMOVAL RUN # ")
print(f"# Run date {datetime.now()} #")
print(f"# Retention set to {RETENTION_DAYS} days")

print("Remove any leftofter fail indicators")
if os.path.exists(FAIL_FILE):
    os.remove(FAIL_FILE)


#work out indexes to keep 
keepDatesList=calculateDatesToKeep(deltaDays=RETENTION_DAYS)
print("\nKeep the following dates by index")
pp.pprint(keepDatesList)


# get indexes
allIndexes=getAllIndexes(fullUrl=ELASTIC_URL,credentials=CREDS)
if not allIndexes:
    touchFileError()
    raise RuntimeError("No indexes found or none returned. Has DeerStack ingested any data?")

# filter anything with deerstack
# results now in split list
# ['yellow open deerstack-7.16.2-02.04.2022     2W8nxQu6TrON0OCY9O9MvQ 1 1 28328    0  19.2mb  19.2mb']

deerstackIndexes=[]
for line in allIndexes.split("\n"):
    if "deerstack" in line:
        deerstackIndexes.append(line)

if not deerstackIndexes:
    touchFileError()
    raise ValueError("No deerstack indexes found")

print(f"\n{len(deerstackIndexes)} current deerstack indexes")

# work out what indexes to remove
toRemove=calIndexesRemoval(keepDates=keepDatesList,deerStackIndexes=deerstackIndexes)
if not toRemove:
    print("No indexes to remove this run")
    sys.exit(0)

# continue to remove indexes if we have them
print("\nWill remove indexes")
print(toRemove)

# finally go and remove index
for indexName in toRemove:
    print(f"Request removal of index {indexName}")
    result=(deleteIndexByName(fullUrl=ELASTIC_URL,credentials=CREDS,indexName=indexName))

    try:
        j=json.loads(result)
    except json.JSONDecodeError:
        raise RuntimeError("MALFORMED ELASTIC RESPONSE")

    if not j.get("acknowledged",False):
        print(f"Could not delete index {indexName}")
        touchFileError()
    else:
        print(f"Removed {indexName} successfully")
