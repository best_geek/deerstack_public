#!/bin/bash


#allow time for elastic container to wake up
secs=60
while [ $secs -gt 0 ]; do
   echo -ne "\r"
   printf  "Wait for container up: $secs"
   echo -ne " seconds$pc\033[0K"
   sleep 1
   : $((secs--))
done
echo " "

#set recusrive loop to post setup to index as may not currently be up in the container
MAX_TRIES=120
COUNTER=0
while [ $COUNTER -ne $MAX_TRIES ]; do
	let COUNTER++
	echo $COUNTER
	echo "Setup index command using kibana API [try ${COUNTER}/${MAX_TRIES}]"


	#setup default index pattern
	#use @timestamp field for from zeek logs for indexing pattern
	curl -q -X POST -u elastic:${ELASTIC_PASSWORD} "http://deerstack-kibana:5601/api/saved_objects/index-pattern/my-pattern"  -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d '
	{
	"attributes": {
		"title": "deer*",
		"timeFieldName": "@timestamp"
	}
	}'


	if [[ $? -eq 0 ]]
	then
		break
		echo "Setup index pattern"
	fi

	sleep 1
done



#show status
echo "Elastic status:"
curl -X GET -u elastic:${ELASTIC_PASSWORD} http://deerstack-elastic-docker-1:9200/_cluster/health


tail -f /dev/null